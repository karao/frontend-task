**Instructions to run the code:**

* Make sure that npm and node is installed in computer by running the following commands 


     $ node -v
 
     $ npm -v



* If its not installed run the following command.


     $ sudo apt-get install -y nodejs



* To install dependencies run the following command from the project root directory


     $ npm install 



* Run following command to install the frontend dependencies


     $ bower install



* Run following command to run the server


    $ npm start