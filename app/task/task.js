/**
 * Created by dell on 14/5/17.
 */

angular.module('myApp.task',['ngRoute'])

.config(['$routeProvider',function ($routeProvider) {
    $routeProvider.when('/task',{
        templateUrl: 'task/task',
        controller: 'TaskCtrl'
    })
}])

.factory()

.controller('TaskCtrl',['$scope', '$parse', function ($scope, $parse) {

    $scope.csv = {
        content: null,
        header: false,
        headerVisible: true,
        separator: ',',
        separatorVisible: false,
        result: null,
        uploadButtonLabel: "upload a csv file"
    };


    $scope.columns = function(row){
        var series = [];
        var values = [];
        var labels = [];


        for(var key in row) {
            series.push(row[key]);
        }

        for(var index = 1; index < series.length - 1; index++){
            var cell = series[index].split('|');
            labels.push(cell[0]);
            values.push(cell[1]);
        }

        return {header: series[0], labels: labels, values: values};
    }
}]);
